
from django.contrib import admin
from django.urls import path

from crmdjango.views import home, add_user

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name="home"),
    path('add_user/', add_user, name="add_user")
]
